import { AbstractControl } from '@angular/forms';

export class CustomValidator {

    static isNumber(control: AbstractControl) {
      const val = control.value;
        if (val === null || val === '') {
            return null;
        }
        if (!val.toString().match(/^[0-9]+(\.?[0-9]+)?$/)) {
        return { 'invalidNumber': true };
        } else {
            return null;
        }
    }
}
