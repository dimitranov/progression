import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

import { MatSnackBar } from '@angular/material';
import { ToastrService } from 'ngx-toastr';

import { UserService } from '../../services/user.service';
import { CustomValidator } from './isNumber';

@Component({
  selector: 'app-measurements-form',
  templateUrl: './measurements-form.component.html',
  styleUrls: ['./measurements-form.component.css']
})
export class MeasurementsFormComponent implements OnInit {
  @Input() innerSubmit;

  measurmentsForm: FormGroup;
  ingeterRegEx = /^-?\d*\.?\d*$/;
  formValid = false;
  formVisible = true;

  constructor(
    private user: UserService,
    private toastr: ToastrService,
    private fb: FormBuilder,
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.measurmentsForm = this.fb.group({
      neck: ['', [Validators.required, CustomValidator.isNumber]],
      chest: ['', [Validators.required, CustomValidator.isNumber]],
      arms: ['', [Validators.required, CustomValidator.isNumber]],
      waist: ['', [Validators.required, CustomValidator.isNumber]],
      hip: ['', [Validators.required, CustomValidator.isNumber]],
      thigh: ['', [Validators.required, CustomValidator.isNumber]],
      calves: ['', [Validators.required, CustomValidator.isNumber]],
    });
  }

  postNewMeasurements(): void {
    if (this.measurmentsForm.status === 'VALID') {
      this.user.postUserMeasurements(this.measurmentsForm.value)
        .subscribe(mes => {
          this.formVisible = false;
          this.toastr.success('New measurements saved.', 'Well done!', {
            timeOut: 3000,
            progressBar: true,
            easing: 'ease-in'
          });
          console.log(mes);
        });
    } else {
      this.measurmentsForm.markAsDirty();
      this.toastr.error('Please fill the form correctly.', 'Warning!', {
        timeOut: 3000,
        progressBar: true,
        easing: 'ease-in'
      });
    }
  }
}

