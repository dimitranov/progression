import { Component, OnInit, Directive, Input, ViewChild, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar, MatStepper } from '@angular/material';
import { MeasurementsFormComponent } from './measurements-form/measurements-form.component';
import { Router } from '@angular/router';

import { UserService } from './../services/user.service';

@Component({
  selector: 'app-user-data-adding',
  templateUrl: './user-data-adding.component.html',
  styleUrls: ['./user-data-adding.component.css']
})

export class UserDataAddingComponent implements OnInit, AfterViewInit {

  @ViewChild(MatStepper ) stepper: MatStepper ;
  @ViewChild(MeasurementsFormComponent) childForm: MeasurementsFormComponent ;

  userDetailsForm: FormGroup;
  ActivityType: string[];
  ingeterRegEx: RegExp;

  nameValid: boolean;
  ageValid: boolean;
  activityValid: boolean;
  weightValid: boolean;
  heightValid: boolean;

  nameValidMessage: string;
  ageValidMessage: string;
  activityValidMessage: string;
  weightValidMessage: string;
  heightValidMessage: string;
  detailsCompleted = false;

  constructor(
    private userService: UserService,
    public snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
  ) {
    this.ActivityType = ['active', 'moderate', 'little', 'unactive'];
    this.ingeterRegEx = /^-?\d*\.?\d*$/;
    this.nameValid = true;
    this.ageValid = true;
    this.activityValid = true;
    this.weightValid = true;
    this.heightValid = true;

  }

  ngOnInit(): void {
    this.createForm();
    this.onChangesName();
    this.onChangesAge();
    this.onChangesWeight();
    this.onChangesHeight();
  }

  ngAfterViewInit(): void {
  }

  createForm(): void {
    this.userDetailsForm = this.fb.group({
      name: ['', Validators.required],
      age: ['', Validators.required],
      activity: ['moderate', Validators.required],
      weight: ['', Validators.required],
      height: ['', Validators.required],
      gender: 'male',
      goals: 'gain-muscule'
    });
  }

  // validate name
  onChangesName(): void {
    this.userDetailsForm.get('name').valueChanges.subscribe(name => {
      if (name.length < 5) {
        this.nameValid = true;
        this.nameValidMessage = 'Must be at least 5 charecters long.';
      } else {
        this.nameValid = false;
        this.nameValidMessage = 'Good';
      }
    });
  }
  // validate age
  onChangesAge(): void {
    this.userDetailsForm.get('age').valueChanges.subscribe(age => {
      // age validation
      if (!this.ingeterRegEx.test(age)) {
        this.ageValid = true;
        this.ageValidMessage = 'Please input only numbers';
      } else if (age.length >= 3 || age.length < 1 || age > 100 || age < 15) {
        this.ageValid = true;
        this.ageValidMessage = 'Please input your age correctly (between 15 and 100)';
      } else {
        this.ageValid = false;
        this.ageValidMessage = 'Good';
      }
    });
  }
  // validate weight
  onChangesWeight(): void {
    this.userDetailsForm.get('weight').valueChanges.subscribe(weight => {
      // age validation
      if (!this.ingeterRegEx.test(weight)) {
        this.weightValid = true;
        this.weightValidMessage = 'Please input only numbers';
      } else if (weight.length >= 3 || weight.length < 2 || weight > 150 || weight < 40) {
        this.weightValid = true;
        this.weightValidMessage = 'Please input your weight correctly (between 40 and 150) ';
      } else {
        this.weightValid = false;
        this.weightValidMessage = 'Good';
      }
    });
  }
  // validate height
  onChangesHeight(): void {
    this.userDetailsForm.get('height').valueChanges.subscribe(height => {
      if (!this.ingeterRegEx.test(height)) {
        this.heightValid = true;
        this.heightValidMessage = 'Please input only numbers';
      } else if (height.length >= 4 || height.length < 3 || height > 220 || height < 140) {
        this.heightValid = true;
        this.heightValidMessage = 'Please input your height correctly (between 140 and 220) ';
      } else {
        this.heightValid = false;
        this.heightValidMessage = 'Good';
      }
    });
  }

  calculateCalories(type: string): number {
    let result = 0,
        activityCoeficient = 0,
        resultTemp = 0;

    const weight = +this.userDetailsForm.get('weight').value,
      height = +this.userDetailsForm.get('height').value,
      age = +this.userDetailsForm.get('age').value,
      activity = this.userDetailsForm.get('activity').value,
      gender = this.userDetailsForm.get('gender').value;
    switch (activity) {
      case 'active':
        activityCoeficient = 1.5;
        break;
      case 'moderate':
        activityCoeficient = 1.3;
        break;
      case 'little':
        activityCoeficient = 1.2;
        break;
      case 'unactive':
        activityCoeficient = 1;
        break;
      default: activityCoeficient = 1.2;
        break;
    }
    // man || woman formula
    resultTemp =
      gender === 'male' ?
      (66.5 + (13.75 * weight) + (5 * height) - (6.77 * age)) * activityCoeficient :
      (665 + (9.56 * weight) + (1.85 * height) - (4.67 * age)) * activityCoeficient;
    switch (type) {
      case 'lose':
        result = resultTemp * 0.75;
        break;
      case 'keep':
        result = resultTemp;
        break;
      case 'gain':
        result = resultTemp + 300;
        break;
      default: result = resultTemp;
        break;
    }
    return Math.round(result);
  }

  submitUserDetails(): void {
    if (this.userDetailsForm.status === 'VALID' &&
      !this.heightValid &&
      !this.nameValid &&
      !this.ageValid &&
      !this.weightValid ) {

      const weight = this.userDetailsForm.get('weight').value;
      const height = this.userDetailsForm.get('height').value;
      const tempObj = Object.assign({
          bim: Math.round(weight / ((height / 100) * (height / 100))),
          caloriesLose: this.calculateCalories('lose'),
          caloriesKeep: this.calculateCalories('keep'),
          caloriesGain: this.calculateCalories('gain')
        },
        this.userDetailsForm.value
      );
      this.userService.postUserDetails(tempObj)
        .subscribe(response => {
          console.log(response);
          this.detailsCompleted = true;
          this.stepper.next();
        });
        this.snackBar.open('Well done. No its time to train !!!', 'Okay', {
          duration: 3000,
        });
      } else {
        this.snackBar.open('Fill ths form correctly. Please!', 'Understood', {
          duration: 3000,
        });
    }
  }
}


