import {
  Component,
  OnInit,
  Input,
  OnChanges
} from '@angular/core';

import {
  CapitalizePipe
} from './../../pipes/capitalize.pipe';
import {
  ExerciseNameFormatingPipe
} from './../../pipes/exercise-name-formating.pipe';
import {
  Exercise
} from './../../types/workout-type';



@Component({
  selector: 'app-group-exercises-view',
  templateUrl: './group-exercises-view.component.html',
  styleUrls: ['./group-exercises-view.component.css']
})
export class GroupExercisesViewComponent implements OnInit, OnChanges {
  @Input() exerciseArray;

  arrayForView: Exercise[];

  constructor() {
    this.arrayForView = [];
  }

  ngOnChanges(): void {
    console.log(this.exerciseArray);
  }

  findUniqueName(arr: Exercise[]): string[] {
    const obj = {};
    for (const item of arr) {
      obj[item.name] = true;
    }
    return Object.keys(obj);
  }

  filter(): void {
    const exercises = this.findUniqueName(this.exerciseArray);
    for (let i = 0; i < exercises.length; i++) {
      this.arrayForView.push(this.exerciseArray.filter(obj => obj.name === exercises[i]));
    }
  }

  ngOnInit(): void {
    this.filter();
  }

}
