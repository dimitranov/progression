import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { DateBeautifyPipe } from './../pipes/date-beautify.pipe';
import { FetchingDataService } from './../services/fetching-data.service';
import { Exercise, Workout } from './../types/workout-type';

@Component({
  selector: 'app-workout-page',
  templateUrl: './workout-page.component.html',
  styleUrls: ['./workout-page.component.css']
})
export class WorkoutPageComponent implements OnInit {
  exercisesdOnDay: Exercise[];
  majonExerciseArray: Exercise[];
  miniorExerciseArray: Exercise[];
  additionalExerciseArray: Exercise[];
  workoutOfToday: Workout;
  readyState1: boolean;
  readyState2: boolean;
  dateFormUrl: string;
  groups: string[];

  constructor(
    private fetch: FetchingDataService,
    private path: ActivatedRoute,
  ) {
    this.readyState1 = false;
    this.readyState2 = false;
    this.dateFormUrl = this.path.snapshot.paramMap.get('date');
  }

  ngOnInit(): void {
    this.fetchData();
    this.fetchWorkoutOfDate();
  }

  findUniquetarget(arr: Exercise[]): string[] {
    const obj = {};
    for (const item of arr) {
      obj[item.target] = true;
    }
    return Object.keys(obj);
  }

  fetchData(): void {
    this.fetch.getExercisesBasedOnDate(this.dateFormUrl)
      .subscribe(response => {
        this.exercisesdOnDay = response;
        this.groups = this.findUniquetarget(this.exercisesdOnDay);
        this.majonExerciseArray = this.exercisesdOnDay.filter(obj => obj.target === this.groups[0]);
        this.miniorExerciseArray = this.exercisesdOnDay.filter(obj => obj.target === this.groups[1]);
        this.additionalExerciseArray = this.exercisesdOnDay.filter(obj => obj.target === this.groups[2]);
        this.readyState1 = true;
      });
  }

  fetchWorkoutOfDate(): void {
    this.fetch.getWorkoutOfDate(this.dateFormUrl)
      .subscribe(response => {
        this.workoutOfToday = response[0];
        this.readyState2 = true;
      });
  }
}

