import { Component, OnInit, Input, ChangeDetectorRef, OnDestroy } from '@angular/core';

import { ExpForStatePipe } from './../../../pipes/exp-for-state.pipe';
import { DateOnlyDatePipe } from './../../../pipes/date-only-date.pipe';
import { HomeClassGroupStatePipe } from './../../../pipes/home-class-group-state.pipe';
import { CapitalizePipe } from './../../../pipes/capitalize.pipe';
import { DateBeautifyPipe } from './../../../pipes/date-beautify.pipe';

declare var $: any;

@Component({
  selector: 'app-single-group-recovery',
  templateUrl: './single-group-recovery.component.html',
  styleUrls: ['./single-group-recovery.component.scss']
})
export class SingleGroupRecoveryComponent implements OnInit, OnDestroy {
  @Input() reciveryObj;
  @Input() date;
  @Input() fromHome;

  classArray: string[];
  datesArray: string[];
  seconds: number;
  minutes: number;
  hours: number;
  days: number;
  isReadyToTrain: boolean;
  timer;

  constructor(private ref: ChangeDetectorRef) {
    this.classArray = [];
    this.datesArray = [];
    this.isReadyToTrain = false;
   }

   ngOnInit(): void {
     $(function () {
       $('[data-toggle="popover"]').popover();
     });
     this.filldateArray();
     this.createClassNameArray();
     const dateEnds = new Date(this.reciveryObj.readyDate.split('-').join(' '));
     this.timer = setInterval(() => {
       const now = new Date();
       const difference = dateEnds.getTime() - now.getTime();
       if (difference <= 0) {
         this.isReadyToTrain = true;
         clearInterval(this.timer);
       } else {
         let seconds = Math.floor(difference / 1000);
         let minutes = Math.floor(seconds / 60);
         let hours = Math.floor(minutes / 60);
         const days = Math.floor(hours / 24);

         hours %= 24;
         minutes %= 60;
         seconds %= 60;

         this.seconds = seconds;
         this.minutes = minutes;
         this.hours = hours;
         this.days = days;

         this.ref.detectChanges();
       }
     }, 1000);
   }

  filldateArray(): void {
    this.datesArray[0] = new Date().toString().substring(4, 15).split(' ').join('-');
    for (let i = 1; i < 6; i++) {
      const OneDayInMS: number = 1000 * 60 * 60 * 24;
      const date: string = new Date(new Date().getTime() + OneDayInMS * i).toString().substring(4, 15).split(' ').join('-');
      this.datesArray.push(date);
    }
    // console.log(this.datesArray);
    // console.log('the index of recovery  date is', this.datesArray.indexOf(this.reciveryObj.readyDate) );
  }

  createClassNameArray(): void {
    switch (this.datesArray.indexOf(this.reciveryObj.readyDate)) {
      case 5:
        this.classArray = [ '00', '0', '1', '2', '3' ];
        break;
      case 4:
        this.classArray = [ '0', '1', '2', '3', '4' ];
        break;
      case 3:
        this.classArray = [ '1', '2', '3', '4', '44' ];
        break;
      case 2:
        this.classArray = [ '2', '3', '4', '44', '44' ];
        break;
      case 1:
        this.classArray = [ '3', '4', '44', '44', '44' ];
        break;
      case 0:
      case -1:
        this.classArray = [ '4', '44', '44', '44', '44' ];
        break;
    default: break;
    }
  }

  ngOnDestroy() {
    clearInterval(this.timer);
  }


}
