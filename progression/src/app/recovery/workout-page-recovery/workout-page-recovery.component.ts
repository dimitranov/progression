import { Component, OnInit, Input } from '@angular/core';

import { Workout } from './../../types/workout-type';

@Component({
  selector: 'app-workout-page-recovery',
  templateUrl: './workout-page-recovery.component.html',
  styleUrls: ['./workout-page-recovery.component.css']
})
export class WorkoutPageRecoveryComponent implements OnInit {
  @Input() data: Workout;

  constructor() { }

  ngOnInit() {
  }

}
