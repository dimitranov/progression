import { Component, OnInit, Input, OnDestroy, Output, OnChanges, SimpleChanges, group } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ExerciseMovement } from './../../types/workout-type';
import { FetchingDataService } from './../../services/fetching-data.service';
import { DateBeautifyPipe } from './../../pipes/date-beautify.pipe';
import { ExerciseNameFormatingPipe } from './../../pipes/exercise-name-formating.pipe';
import { ExercisePickingService } from './../../services/exercise-picking.service';
import { Exercise } from '../../types/workout-type';

@Component({
  selector: 'app-exercise-from',
  templateUrl: './exercise-from.component.html',
  styleUrls: ['./exercise-from.component.css']
})

export class ExerciseFromComponent implements OnInit, OnDestroy, OnChanges {
  @Input() workouts;
  @Input() exercisesArray;
  @Input() workoutSession;
  @Input() count;

  form: FormGroup;
  repsValid: boolean;
  ingeterRegEx: RegExp;
  repsValidMessage: string;
  weightValid: boolean;
  weightValidMessage: string;

  exerciseMatrix: Exercise[][];
  passTimeTrainingData: Exercise[];
  localeArrayForExerciseSets = [];
  exerciseDetails: ExerciseMovement;
  uniqueDates: string[];
  exercise: string;
  imageURL: string;
  exerciseIsPicked: boolean;
  repsAreInputed: boolean;

  constructor(
    private fetch: FetchingDataService,
    private sesCreateServ: ExercisePickingService,
    private fb: FormBuilder,
  ) {
    this.exerciseIsPicked = true;
    this.repsAreInputed = true;
    this.createForm();
    this.ingeterRegEx = /^-?\d*\.?\d*$/;
    this.workouts = [];
    this.exerciseMatrix = [];
  }

  ngOnInit(): void {
    if (!localStorage.getItem(`localArrayForGroup${this.count}`)) {
      localStorage.setItem(`localArrayForGroup${this.count}`, JSON.stringify([]));
    } else {
      this.localeArrayForExerciseSets = JSON.parse(localStorage.getItem(`localArrayForGroup${this.count}`));
    }
    this.onExercisePick();
    this.onChangesWeight();
    this.onChangesReps();
  }
  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'workouts') {
        const cur = JSON.stringify(changes.workouts.currentValue);
        const prev = JSON.stringify(changes.workouts.previousValue);
        if (cur !== prev) {
          this.findLastDateOfWorkoutForGroup();
          // execute get  with target=chest&date=Apr-23-2018
          // pass ass prop to exercisesesion component
        }
      }
    }
  }
  onExercisePick(): void {
    this.form.get('groupExercise').valueChanges.subscribe(exercise => {
      this.fetchAllExercisePractices(exercise);
      const exer = this.form.get('groupExercise').value;
      this.fetch.getExerciseData(exer)
        .subscribe(e => this.imageURL = e[0].imageURL);
    });
  }
  findAllUniqueDates(arr: Exercise[]): string[] {
    const obj = {};
    for (const item of arr) {
      obj[item.date] = true;
    }
    return Object.keys(obj);
  }

  fetchAllExercisePractices(exercise: string): void {
    this.fetch.getAllExercisePractices(exercise)
      .subscribe(response => {
        this.exerciseMatrix = [];
        this.uniqueDates = this.findAllUniqueDates(response);
        this.uniqueDates.forEach(date => {
          this.exerciseMatrix.push(response.filter(obj => obj.date === date));
        });
        this.fetchExericseData(this.exercise);
      });
  }

  fetchExericseData(exercise: string): void {
    this.fetch.getExerciseData(exercise)
      .subscribe(response => {
        this.exerciseDetails = response[0];
      });
  }

  findLastDateOfWorkoutForGroup(): void {
    let obj;
    for (let i = 0; i < this.workouts.length - 1; i++) {
      for (let j = 0; j < this.workouts[i].session.length; j++) {
        if (this.workouts[i].session[j] === this.workoutSession.session[this.count]) {
          obj = {
            date: this.workouts[i].date,
            group: this.workoutSession.session[this.count]
          };
          break;
        }
      }
    }
    this.sesCreateServ.getExerciseArrayByNameAndDate(obj)
      .subscribe(response => {
        this.passTimeTrainingData = response;
      });
  }
  onChangesWeight(): void {
    this.form.get('weight').valueChanges.subscribe(weight => {
      // weight validation
      if (!this.ingeterRegEx.test(weight)) {
        this.weightValid = true;
        this.weightValidMessage = 'Please input only numbers';
      } else if (weight > 500) {
        this.weightValid = true;
        this.weightValidMessage = 'Please input legit weight';
      } else {
        this.weightValid = false;
        this.weightValidMessage = 'Good';
      }
    });
  }
  onChangesReps(): void {
    this.form.get('reps').valueChanges.subscribe(reps => {
      // reps validation
      if (!this.ingeterRegEx.test(reps)) {
        this.repsValid = true;
        this.repsValidMessage = 'Please input only numbers';
      } else if (reps > 200 || !reps.length) {
        this.repsValid = true;
        this.repsValidMessage = 'Please input legit reps count';
      } else {
        this.repsValid = false;
        this.repsValidMessage = 'Good';
      }
    });
  }

  ngOnDestroy(): void {
    localStorage.removeItem(`localArrayForGroup${this.count}`);
  }

  createForm(): void {
    this.form = this.fb.group({
      groupExercise: ['', Validators.required],
      reps: ['', Validators.required],
      weight: ''
    });
  }

  addSet(): void {
    if (this.form.status === 'VALID') {
      this.exerciseIsPicked = true;
      this.repsAreInputed = true;

      const obj = {
        name: this.form.get('groupExercise').value,
        date: this.workoutSession.date,
        reps: this.form.get('reps').value,
        weight: this.form.get('weight').value,
        target: this.exercisesArray[0].target
      };

      this.sesCreateServ.postNewExerciseWithSets(obj)
        .subscribe(newExercise => {
          // console.log('new Exercise added: ', newExercise);
          const retrievedObject = localStorage.getItem(`localArrayForGroup${this.count}`);
          const tempArr = JSON.parse(retrievedObject);
          tempArr.push(newExercise);
          localStorage.setItem(`localArrayForGroup${this.count}`, JSON.stringify(tempArr));
          this.localeArrayForExerciseSets.push(newExercise);
        });
      // console.log('localeArrayForExerciseSets ', this.localeArrayForExerciseSets);
    } else {
      this.exerciseIsPicked = false;
      this.repsAreInputed = false;
    }
  }

  deleteExercise(id: number) {
    this.sesCreateServ.deleteExerciseRecordByID(id)
      .subscribe(response => {
        const retrievedObject = localStorage.getItem(`localArrayForGroup${this.count}`);
        let tempArr = JSON.parse(retrievedObject);
        tempArr = tempArr.filter(obj => obj.id !== id);
        localStorage.setItem(`localArrayForGroup${this.count}`, JSON.stringify(tempArr));
        this.localeArrayForExerciseSets = tempArr;
      });
  }
}

