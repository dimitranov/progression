import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { ToastrService } from 'ngx-toastr';

import { ExercisePickingService } from './../services/exercise-picking.service';
import { CapitalizePipe } from './../pipes/capitalize.pipe';
import { ExerciseNameFormatingPipe } from './../pipes/exercise-name-formating.pipe';
import { Workout, GroupSpecificExercise } from './../types/workout-type';
import { WorkoutDecidingService } from './../services/workout-deciding.service';


@Component({
  selector: 'app-add-session',
  templateUrl: './add-session.component.html',
  styleUrls: ['./add-session.component.css']
})
export class AddSessionComponent implements OnInit {
  workouts: Workout[];
  workoutSession: Workout;
  groupOneExercises: GroupSpecificExercise[];
  groupTwoExercises: GroupSpecificExercise[];
  groupThreeExercises: GroupSpecificExercise[];
  tempexercisesForGroup3: GroupSpecificExercise[];
  allreadyFlag = false;

  constructor(
    private workoutServ: WorkoutDecidingService,
    private exercisePicking: ExercisePickingService,
    public toastr: ToastrService,
    private router: Router,
  ) {
      this.getWorkoutSession();
    }

  ngOnInit(): void {
    this.workoutServ.getAllWorkouts().subscribe(res => {
      this.workouts = res;
      this.allreadyFlag = true;
    });
  }


  getWorkoutSession(): void {
    this.workoutServ.getWorkoutSesionOfToday()
      .subscribe(response => {
        if (response[0]) {
          this.workoutSession = response[0];
          this.exercisePicking.getExercisesForGroup(this.workoutSession.session[0])
          .subscribe(array => {
            this.groupOneExercises = array;
          });

          this.exercisePicking.getExercisesForGroup(this.workoutSession.session[1])
          .subscribe(array => {
            this.groupTwoExercises = array;
          });
          this.exercisePicking.getExercisesForGroup(this.workoutSession.session[2])
          .subscribe(array => {
            this.groupThreeExercises = array;
          });
        }
      });
  }

  abandonWorkout(today): void {
    localStorage.clear();
    this.workoutServ.abdandonWorkout(this.workoutSession.id)
      .subscribe(res => this.router.navigate(['/home']));
      this.exercisePicking.getAllRecordsFromDotayforDeletion(today)
        .subscribe(responseArray => {
          responseArray.forEach(obj => {
            this.exercisePicking.deleteExerciseRecordByID(obj.id)
           .subscribe(result => this.toastr.info('Workout Abandoned.'));
          });
        });
  }

  saveWorkout(): void {
    const newWorkoutSession = this.workoutSession;
    newWorkoutSession['completed'] = true;
    this.workoutServ.patchWorkoutOnComplete(newWorkoutSession.id, newWorkoutSession)
    .subscribe(resl => {
      this.toastr.success('Workout completed successfully. Well done!', 'Success!',  {
        progressBar: true,
        timeOut: 4000,
      });
      localStorage.clear();
      this.router.navigate(['/home']);
    });
  }

}
