import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-measurements-chart',
  templateUrl: './measurements-chart.component.html',
  styleUrls: ['./measurements-chart.component.css']
})
export class MeasurementsChartComponent implements OnInit {
  @Input() data; // [{data:[1,2,4], label: string,}]
  @Input() label; // string[]

  public lineChartOptions: any = {
    responsive: true,
  };

  public lineChartLegend = true;

  public lineChartColors: Array<any> = [{
      backgroundColor: 'rgba(85, 255, 193, 0.3)',
      borderColor: 'rgb(50, 185, 136)',
      pointBackgroundColor: 'rgb(50, 185, 136)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }];

  constructor() { }

  ngOnInit() {
  }

}
