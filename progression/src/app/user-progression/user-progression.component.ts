import { Component, OnInit } from '@angular/core';

import { FetchingDataService } from './../services/fetching-data.service';

@Component({
  selector: 'app-user-progression',
  templateUrl: './user-progression.component.html',
  styleUrls: ['./user-progression.component.css']
})
export class UserProgressionComponent implements OnInit {
  public dataForChartIsReady = false;
  public lineChartData: any[][];
  public lineChartLabels: string[];
  public lineChartOptions: any = {
    responsive: true,
  };
  constructor(private fetch: FetchingDataService) {
    this.lineChartData = [];
  }

  ngOnInit(): void {
    this.fetchMeasurements();
  }

  formatDates(date: string): string {
    return `${date.split('-')[0]} ${date.split('-')[1]}`;
  }

  fetchMeasurements(): void {
    this.fetch.getMeasurementsArray()
      .subscribe(response => {
        this.lineChartLabels = response.map(o => this.formatDates(o.date));
        const mearuemrntsUnits = ['neck', 'waist', 'chest', 'arms', 'calves', 'hip', 'thigh'];
        for (const entity of mearuemrntsUnits) {
          this.lineChartData.push([{
            data: response.map(o => o[entity]),
            label: entity.toUpperCase(),
          }]);
        }
        this.dataForChartIsReady = true;
      });
  }
}

