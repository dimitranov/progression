import { Component, OnInit, ChangeDetectorRef, OnChanges, SimpleChange, SimpleChanges } from '@angular/core';

import { Workout } from './../../types/workout-type';
import { FetchingDataService } from './../../services/fetching-data.service';

@Component({
  selector: 'app-calendar-workouts',
  templateUrl: './calendar-workouts.component.html',
  styleUrls: ['./calendar-workouts.component.scss']
})

export class CalendarWorkoutsComponent implements OnInit {
  today: string;
  dates: string[];
  allWokrouts: Workout[];
  arrayForView: any[] = [];
  indexArray: number[] = [];
  musculesArr: string[];
  weeksShiftingCount: number;
  numberOfWeeksToDisplay: number;

  constructor(
    private fetch: FetchingDataService,
    private ref: ChangeDetectorRef
  ) {
    this.today =
      new Date().toString().substring(4, 15).split(' ').join('-');
    this.weeksShiftingCount = 3;
    this.numberOfWeeksToDisplay = 4;
    this.musculesArr = ['chest', 'back', 'shoulders', 'legs', 'biceps', 'triceps', 'traps', 'abbs', 'calves', 'forearms', ];
  }
  ngOnInit(): void {
    this.calculateDatesAndArray(this.weeksShiftingCount, this.numberOfWeeksToDisplay);
  }

  calculateDatesAndArray(weeksShiftingCount: number, weeks: number): void {
    console.log(weeksShiftingCount, weeks);

    const tempDates = [];
    const oneDayeInMS = 1000 * 60 * 60 * 24;

    const todayAsNumber = new Date().getDay();

    const dateOfLastMonday = new Date(Date.now() - 24 * 3600 * 1000 * (todayAsNumber - 1));

    const dateBeforeThreeWeeksFromLastMonday =
      new Date(Date.now() - 24 * 3600 * 1000 * ((weeksShiftingCount * 7) + dateOfLastMonday.getDay()));

    for (let i = 0; i < (weeks * 7); i++) {
      tempDates.push(
        new Date(dateBeforeThreeWeeksFromLastMonday.getTime() + (oneDayeInMS * i)).toString().substring(4, 15).split(' ').join('-'));
    }
    this.dates = tempDates;
    this.fetchAllWorkouts();
  }

  // fetches all  workouts from db
  fetchAllWorkouts(): void {
    this.fetch.getAllWorkots().subscribe(res => {
      this.allWokrouts = res;
      this.generateViewArray();
    });
  }

  // generated array of objects based on this.dates
  generateViewArray(): void {
    this.arrayForView = [];
    this.dates.forEach(date => {
      if (this.allWokrouts.find(unit => unit.date === date)) {
        this.arrayForView.push(this.allWokrouts.find(unit => unit.date === date));
      } else {
        this.arrayForView.push({
          date,
          rest: true
        });
      }
    });
  }

  // pulls and updates weeksShiftingCount with a passed week from DB
  calendarPast(): void {
    this.calculateDatesAndArray(
      this.weeksShiftingCount + 1,
      this.numberOfWeeksToDisplay
    );
    this.weeksShiftingCount = this.weeksShiftingCount + 1;
    this.ref.detectChanges();
    this.indexArray = [];
  }

  // pulls and updates weeksShiftingCount with a future week from DB
  calendarFuture(): void {
    this.calculateDatesAndArray(
      this.weeksShiftingCount - 1,
      this.numberOfWeeksToDisplay
    );
    this.weeksShiftingCount = this.weeksShiftingCount - 1;
    this.ref.detectChanges();
    this.indexArray = [];
  }

  // finds the indexes of all object containing group in there session param
  filterByGroup(group: string): void {
    const indexArray = [];
    for (let index = 0; index < this.arrayForView.length; index++) {
      if (this.arrayForView[index].session && this.arrayForView[index].session.find(st => st === group)) {
        indexArray.push(index);
      }
    }
    console.log(indexArray);
    this.indexArray = indexArray;
    this.ref.detectChanges();
  }

  // returns true if i is present in indexArray
  arrIncludes(i: number): boolean {
    for (let index = 0; index < this.indexArray.length; index++) {
      if (this.indexArray.find(e => e === i)) {
        return true;
      }
    }
  }
}

