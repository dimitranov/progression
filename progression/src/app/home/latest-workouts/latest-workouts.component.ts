import { Component, OnInit, Input } from '@angular/core';

import { CapitalizePipe } from './../../pipes/capitalize.pipe';
import { DateBeautifyPipe } from './../../pipes/date-beautify.pipe';

@Component({
  selector: 'app-latest-workouts',
  templateUrl: './latest-workouts.component.html',
  styleUrls: ['./latest-workouts.component.css']
})
export class LatestWorkoutsComponent implements OnInit {
  @Input() loadedStatus;
  @Input() allWorkoutsInPassWeekend;
  @Input() title;

  constructor() {}

  ngOnInit() {}

}

