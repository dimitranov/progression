import { Component, OnInit, group } from '@angular/core';
import { Router } from '@angular/router';

import { Recovery, Workout } from './../types/workout-type';
import { WorkoutDecidingService } from './../services/workout-deciding.service';
import { CapitalizePipe } from './../pipes/capitalize.pipe';
import { DateBeautifyPipe } from './../pipes/date-beautify.pipe';
import { HomePageService } from '../services/home-page.service';
import { UserService } from '../services/user.service';
import { User } from '../types/user';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  allWorkoutsInPassWeekend: Workout[];
  allGroupsRecoveryArray: Recovery[];
  loadedStatus: boolean;
  userName: string;
  userObj: User;
  unlockWorkoutButton = false;
  backToSessionUnlock = false;


  constructor(
    private user: UserService,
    private homeServ: HomePageService,
    private workoutServ: WorkoutDecidingService,
    private router: Router,
  ) {
    this.loadedStatus = false;
   }


  ngOnInit(): void {
    this.fetchAllWorkoutsFromPassWeek();
    this.workoutServ.getWorkoutSesionOfToday()
    .subscribe(r => {
      console.log(r);
       this.unlockWorkoutButton = r.length === 0;
        this.backToSessionUnlock =   r.length !== 0 && !r[0].completed;
    });
    this.user.getUser().subscribe(user => {
      this.userObj = user;
    });
  }

  fetchAllWorkoutsFromPassWeek(): void {
    const  todayUnFormated = new Date();
    const dateArray: string[] = [];
    for (let i = 0; i < 7; i++) {
      const last = new Date(todayUnFormated.getTime() - (i * 24 * 60 * 60 * 1000)),
            day = last.getDate().toString().length === 2 ? last.getDate() : `0${last.getDate()}`,
            month = last.getMonth() + 1,
            year = last.getFullYear();
      let monthStr = '';
      switch (month) {
        case 1: monthStr = 'Jan'; break;
        case 2: monthStr = 'Feb'; break;
        case 3: monthStr = 'Mar'; break;
        case 4: monthStr = 'Apr'; break;
        case 5: monthStr = 'May'; break;
        case 6: monthStr = 'Jun'; break;
        case 7: monthStr = 'Jul'; break;
        case 8: monthStr = 'Aug'; break;
        case 9: monthStr = 'Sep'; break;
        case 10: monthStr = 'Oct'; break;
        case 11: monthStr = 'Nov'; break;
        case 12: monthStr = 'Dec'; break;
      }
      dateArray[i] = `${monthStr}-${day}-${year}`;
    }
    this.homeServ.fetchAllWorkoutsFromPassWeek(dateArray)
      .subscribe(response => {
        this.loadedStatus = true;
        this.allWorkoutsInPassWeekend = response.reverse();
        this.createAllGroupsRecoveryArray();
      });
  }
  createAllGroupsRecoveryArray(): void {
    const workoutsArray: Workout[] = this.allWorkoutsInPassWeekend;
    const tempRecoveryArr: Recovery[] = [];
    const today = new Date().toString().substring(4, 15).split(' ').join('-');
    for (let i = 0; i < workoutsArray.length; i++) {
      for (let j = 0; j < workoutsArray[i].recovery.length; j++) {
        if (!tempRecoveryArr.find(o => o.group === workoutsArray[i].recovery[j].group)) {
          const obj = Object.assign({date: workoutsArray[i].date}, workoutsArray[i].recovery[j]);
          if (obj.group !== '') {
            tempRecoveryArr.push(obj);
          }
        }
      }
    }
    const groups = ['chest', 'back', 'shoulders', 'legs', 'forearms', 'biceps', 'triceps', 'calves', 'traps', 'abbs'];
    groups.filter(g => !tempRecoveryArr.find(item => item.group === g)).forEach(element => {
      const  obj = {
        group: element,
        date: today,
        readyDate: today
     };
      tempRecoveryArr.push(obj);
    });

     this.allGroupsRecoveryArray =
    tempRecoveryArr.sort((a, b) =>
      new Date(a.readyDate).getTime() - new Date(b.readyDate).getTime());
      localStorage.setItem('allGroupsRecoveryArray_LOCALE', JSON.stringify(this.allGroupsRecoveryArray));
  }
}
