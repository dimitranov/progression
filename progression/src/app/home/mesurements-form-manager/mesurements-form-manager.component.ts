import { Component, OnInit } from '@angular/core';

import { UserService } from './../../services/user.service';

@Component({
  selector: 'app-mesurements-form-manager',
  templateUrl: './mesurements-form-manager.component.html',
  styleUrls: ['./mesurements-form-manager.component.css']
})
export class MesurementsFormManagerComponent implements OnInit {

  displayFormForMeasurements = false;

  constructor(
    private user: UserService
  ) {}

  ngOnInit() {
    this.chechIfDayForMeasuremntsHadCame();
  }
  chechIfDayForMeasuremntsHadCame(): void {
    this.user.getLastDateUserInputedMeasurements()
      .subscribe(result => {
        if (result.length > 0) {
          const today = new Date().toString().substring(4, 15).split(' ').join('-');
          const lastObj = result[result.length - 1];
          const dateIFInputInMS = new Date(lastObj.date).getTime();
          const todayInMS = new Date().getTime();
          const sevenDaysInMS = 1000 * 60 * 60 * 24 * 7;
          if (lastObj.date !== today && (todayInMS - dateIFInputInMS) > sevenDaysInMS) {
            this.displayFormForMeasurements = true;
          }
        }
      });
  }
}

