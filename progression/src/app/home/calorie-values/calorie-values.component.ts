import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

import { UserGoals } from './../../types/user';

@Component({
  selector: 'app-calorie-values',
  templateUrl: './calorie-values.component.html',
  styleUrls: ['./calorie-values.component.scss']
})
export class CalorieValuesComponent implements OnInit {
  @Input() user;

  initialGoals: UserGoals[];
  goalsArrFiltered: UserGoals[];
  mainGoalObj: UserGoals;

  constructor() {}

  ngOnInit(): void {
    this.initialGoals = [{
        goalShort: 'Lose:',
        goal: 'lose-fat',
        value: this.user.caloriesLose
      },
      {
        goalShort: 'Keep:',
        goal: 'keep-weight',
        value: this.user.caloriesKeep
      },
      {
        goalShort: 'Gain:',
        goal: 'gain-muscule',
        value: this.user.caloriesGain
      }
    ];

    this.goalsArrFiltered = this.initialGoals.filter(obj => obj.goal !== this.user.goals);
    this.mainGoalObj = this.initialGoals.find(obj => obj.goal === this.user.goals);
  }
}


