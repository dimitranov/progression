import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import 'rxjs/add/operator/catch';

import { Exercise, MusculeGroupSesion, Workout } from './../types/workout-type';

@Injectable()
export class WorkoutDecidingService {

  postWorkoutUrl: string;
  postMusculeGroup: string;
  postExercises: string;
  getTodaysWorkout: string;

  constructor(private http: HttpClient) {
    this.postWorkoutUrl = 'http://localhost:3000/workouts';
    this.postMusculeGroup = 'http://localhost:3000/';
    this.postExercises = 'http://localhost:3000/exercises';
    this.getTodaysWorkout = 'http://localhost:3000/workouts?date=';
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend status ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return new ErrorObservable(
      'Something when wrong.');
  }

  addWorkout(workout): Observable < Workout > {
    return this.http.post < Workout > (this.postWorkoutUrl, workout)
      .pipe(
        catchError(this.handleError)
      );
  }

  addMusculeGroupSession(group: string, workout: MusculeGroupSesion): Observable < MusculeGroupSesion > {
    const url = this.postMusculeGroup + group;
    return this.http.post < MusculeGroupSesion > (url, workout)
      .pipe(
        catchError(this.handleError)
      );
  }

  addExercise(exercise: string): Observable < Exercise > {
    return this.http.post < Exercise > (this.postExercises, exercise)
      .pipe(
        catchError(this.handleError)
      );
  }

  getWorkoutSesionOfToday(): Observable < Workout[] > {
    const url = this.getTodaysWorkout + new Date().toString().substring(4, 15).split(' ').join('-');
    return this.http.get < Workout[] > (url).pipe(
      catchError(this.handleError),
    );
  }

  abdandonWorkout(id: number): Observable < {} > {
    const url = 'http://localhost:3000/workouts/' + id;
    return this.http.delete < {} > (url)
      .pipe(
        catchError(this.handleError)
      );
  }

  patchWorkoutOnComplete(id: number, obj: Workout): Observable < Workout > {
    const url = 'http://localhost:3000/workouts/' + id;
    return this.http.patch < Workout > (url, obj)
      .pipe(
        catchError(this.handleError)
      );
  }

  getAllWorkouts(): Observable < Workout[] > {
    const url = 'http://localhost:3000/workouts';
    return this.http.get < Workout[] > (url).pipe(
      catchError(this.handleError)
    );
  }

}

