import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import 'rxjs/add/operator/catch';

import { UserMeasurements } from '../types/user';
import { Exercise, MusculeGroupSesion, Workout, ExerciseMovement } from './../types/workout-type';

@Injectable()
export class FetchingDataService {

  constructor(private http: HttpClient) {}
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend status ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return new ErrorObservable(
      'Something when wrong.');
  }

  getAllWorkots(): Observable < Workout[] > {
    const url = `http://localhost:3000/workouts`;
    return this.http.get < Workout[] > (url).pipe(
      catchError(this.handleError)
    );
  }

  getExercisesBasedOnDate(date: string): Observable < Exercise[] > {
    const url = `http://localhost:3000/exercises?date=${date}`;
    return this.http.get < Exercise[] > (url).pipe(
      catchError(this.handleError)
    );
  }

  getWorkoutOfDate(date: string): Observable < Workout[] > {
    const url = `http://localhost:3000/workouts?date=${date}`;
    return this.http.get < Workout[] > (url).pipe(
      catchError(this.handleError)
    );
  }

  getAllExercisesData(): Observable < ExerciseMovement[] > {
    const url = 'http://localhost:3000/exercisedb';
    return this.http.get < ExerciseMovement[] > (url).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  getMeasurementsArray(): Observable < UserMeasurements[] > {
    const url = 'http://localhost:3000/user-measurements';
    return this.http.get < UserMeasurements[] > (url).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  getAllExercisePractices(exercise: string): Observable < Exercise[] > {
    const url = `http://localhost:3000/exercises?name=${exercise}`;
    return this.http.get < Exercise[] > (url).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  getExerciseData(exercise: string): Observable < ExerciseMovement > {
    const url = `http://localhost:3000/exercisedb?name=${exercise}`;
    return this.http.get < ExerciseMovement > (url).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  getAllExercisesPerformedBasedOnTarget(target: string): Observable < Exercise[] > {
    const url = `http://localhost:3000/exercises?target=${target}`;
    return this.http.get < Exercise[] > (url).pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

}

