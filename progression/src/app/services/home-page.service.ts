import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';

import { Exercise, MusculeGroupSesion, Workout , GroupSpecificExercise} from './../types/workout-type';
import { User } from '../types/user';

@Injectable()
export class HomePageService {

  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend status ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return new ErrorObservable(
      'Something when wrong.');
  }

  fetchAllWorkoutsFromPassWeek(dateArray: string[]): Observable < Workout[] > {
    const url =
      `http://localhost:3000/workouts?date=${dateArray[0]}&completed=true` +
      `&date=${dateArray[1]}` +
      `&date=${dateArray[2]}` +
      `&date=${dateArray[3]}` +
      `&date=${dateArray[4]}` +
      `&date=${dateArray[5]}` +
      `&date=${dateArray[6]}`;

    return this.http.get < Workout[] > (url)
      .pipe(catchError(this.handleError));
  }

  getUser(): Observable < User > {
    const url = `http://localhost:3000/user`;
    return this.http.get(url).pipe(retry(3), catchError(this.handleError));
  }

}

