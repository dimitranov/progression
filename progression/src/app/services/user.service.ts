import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import 'rxjs/add/operator/catch';

import { User, UserMeasurements } from './../types/user';

@Injectable()
export class UserService {
  userUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'HhGgyurepOjNhbfctriofur'
    })
  };

  constructor(private http: HttpClient) {
    this.userUrl = 'http://localhost:3000/user';
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend status ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return new ErrorObservable(
      'Something when wrong.');
  }

  postUserDetails(userObj: User): Observable < User > {
    return this.http.post < User > (this.userUrl, userObj, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getUser(): Observable < User > {
    return this.http.get < User > (this.userUrl)
      .pipe(
        catchError(this.handleError)
      );
  }

  postUserMeasurements(measurements: UserMeasurements): Observable < UserMeasurements > {
    const url = 'http://localhost:3000/user-measurements';
    const date = new Date().toString().substring(4, 15).split(' ').join('-');
    const obj = Object.assign({
      date
    }, measurements);
    return this.http.post < UserMeasurements > (url, obj, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      );
  }

  getLastDateUserInputedMeasurements(): Observable < UserMeasurements[] > {
    const today = new Date().toString().substring(4, 15).split(' ').join('-');
    const url = `http://localhost:3000/user-measurements`;
    return this.http.get(url)
      .pipe(
        retry(3),
        catchError(this.handleError)
      );
  }
}


