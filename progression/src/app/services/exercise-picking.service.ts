import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';

import { Exercise, MusculeGroupSesion, Workout , GroupSpecificExercise} from './../types/workout-type';

@Injectable()
export class ExercisePickingService {

  getGroupSpecificExerciseUrl: string;
  pathSets: string;

  constructor(private http: HttpClient) {
    this.getGroupSpecificExerciseUrl = 'http://localhost:3000/exercisedb?target=';
    this.pathSets = 'http://localhost:3000/exercises';
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend status ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return new ErrorObservable(
      'Something when wrong.');
  }

  getExercisesForGroup(group):
    Observable < GroupSpecificExercise[] > {
      const url = this.getGroupSpecificExerciseUrl + group;
      return this.http.get < GroupSpecificExercise[] > (url)
        .pipe(catchError(this.handleError));
    }

  postSetData(set): Observable < Exercise > {
    return this.http.post < Exercise > (this.pathSets, set)
      .pipe(catchError(this.handleError));
  }

  chechIfExerciseOnDateExist(name, date): Observable < Exercise > {
    const url = `http://localhost:3000/exercises?name=${name}&date=${date}`;
    return this.http.get < Exercise > (url)
      .pipe(catchError(this.handleError));
  }

  postNewExerciseWithSets(object): Observable < Exercise > {
    return this.http.post < Exercise > (this.pathSets, object)
      .pipe(catchError(this.handleError));
  }

  deleteExerciseRecordByID(id): Observable < {} > {
    const url = `${this.pathSets}/${id}`;
    return this.http.delete < {} > (url)
      .pipe(catchError(this.handleError));
  }

  getAllRecordsFromDotayforDeletion(today): Observable < Exercise[] > {
    const url = `${this.pathSets}?date=${today}`;
    return this.http.get < Exercise[] > (url)
      .pipe(catchError(this.handleError));
  }

  getExerciseArrayByNameAndDate(obj: any):
    Observable < Exercise[] > {
      const url = `http://localhost:3000/exercises?target=${obj.group}&date=${obj.date}`;
      return this.http.get(url)
        .pipe(catchError(this.handleError));
    }

}

