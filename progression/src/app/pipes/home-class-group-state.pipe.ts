import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'homeClassGroupState'
})
export class HomeClassGroupStatePipe implements PipeTransform {

  transform(value: any, type ?: any): any {
    let result: string;
    const classSufix = value.split('_')[2];
    switch (classSufix) {
      case '00':
        result = 'Forbidden';
        break;
      case '0':
        result = 'Warrning';
        break;
      case '1':
        result = 'Not Recomended';
        break;
      case '2':
        result = 'Fairly Recovered';
        break;
      case '3':
        result = 'Ready Soon';
        break;
      case '4':
        result = 'Recovered';
        break;
      case '44':
        result = 'Do it !';
        break;
      default:
        break;
    }
    return result;
  }

}

