import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateBeautify'
})
export class DateBeautifyPipe implements PipeTransform {

  transform(value: any): any { // Apr-16-2018

    const tempValArr = value.split('-');

    const today = new Date().toString().substring(4, 15).split(' ').join('-');

    const dayName = new Date(value).toString().substring(0, 3);
    return value === today ? `Today` : `${dayName} - ${tempValArr[1]} ${tempValArr[0]} ${tempValArr[2]}`;
  }

}

