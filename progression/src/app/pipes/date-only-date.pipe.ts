import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateOnlyDate'
})
export class DateOnlyDatePipe implements PipeTransform {

  transform(value: any, args ?: any): any { // Apr-16-2018
    const today = new Date().toString().substring(4, 15).split(' ').join('-');
    return value === today ? `Today` : value.substring(4, 6) + ' ' + value.substring(0, 3);
  }

}

