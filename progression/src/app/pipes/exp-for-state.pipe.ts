import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'expForState'
})
export class ExpForStatePipe implements PipeTransform {

  transform(value: any, type ?: any): any {
    let result: string;
    const classSufix = value.split('_')[2];
    switch (classSufix) {
      case '00':
        result = 'You just trained, try not to overload the muscle.';
        break;
      case '0':
        result = 'The process of recovery just  started.';
        break;
      case '1':
        result = 'The muscule needs a few more days to recover. ';
        break;
      case '2':
        result = 'You mau feed strong again, but under the hood you muscule needs more rest and nutrition.';
        break;
      case '3':
        result = 'If you feel ready  you  can train. But we recomend you to wait one more day.';
        break;
      case '4':
        result = 'The muscule is filly recovered and can be trained again. Lets do this!';
        break;
      default:
        break;
    }
    return result;
  }

}

