import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatExerciseName'
})
export class ExerciseNameFormatingPipe implements PipeTransform {

  transform(value: string): any {
    const valueRrr = value.split('-');
    for (let index = 0; index < valueRrr.length; index++) {
      valueRrr[index] = valueRrr[index].charAt(0).toUpperCase() + valueRrr[index].slice(1);
    }
    return valueRrr.join(' ');
  }

}

