import { Workout } from './../types/workout-type';
import { ActivatedRoute, Router } from '@angular/router';
import { FetchingDataService } from './../services/fetching-data.service';
import { Component, OnInit } from '@angular/core';
import { Exercise } from '../types/workout-type';

@Component({
  selector: 'app-muscule-development',
  templateUrl: './muscule-development.component.html',
  styleUrls: ['./muscule-development.component.css']
})
export class MusculeDevelopmentComponent implements OnInit {
  Exercise3D: Exercise[][][];
  workoutsWithGroupArray: Workout[];
  group: string;
  loadedStatus = false;

  constructor(
    private fetch: FetchingDataService,
    private route: ActivatedRoute,
    private router: Router) {
      this.group = this.route.snapshot.paramMap.get('group');
    }

  ngOnInit(): void {
    this.fetchAllExercisesPerformedBasedOnTarget();
    this.fetchAllWorkouts();
  }

  findAllUniqueNames(arr: any[]): string[] {
    const obj = {};
    for (const item of arr) {
      obj[item.name] = true;
    }
    return Object.keys(obj);
  }

  findAllUniqueDates(arr: any[]): string[] {
    const obj = {};
    for (const item of arr) {
      obj[item.date] = true;
    }
    return Object.keys(obj);
  }

  fetchAllExercisesPerformedBasedOnTarget(): void {
    this.fetch.getAllExercisesPerformedBasedOnTarget(this.group)
    .subscribe(res => {
      const uniqueExercises = this.findAllUniqueNames(res);
      const exercise3 = [];
      uniqueExercises.forEach(name => {
        const firstFiltered = res.filter(obj => obj.name === name);
        const exercise2 = [];
        const uniqueDates = this.findAllUniqueDates(firstFiltered);
        uniqueDates.forEach(date => {
          exercise2.push(firstFiltered.filter(obj => obj.date === date));
        });
        exercise3.push(exercise2);
      });
      this.Exercise3D = exercise3;
    });
  }

  fetchAllWorkouts(): void {
    this.fetch.getAllWorkots().subscribe(workouts => {
      // filter and find only  where gorup is included in  session property array
      this.workoutsWithGroupArray = workouts.filter(unit => unit.session.includes(this.group)).reverse();
      this.loadedStatus = true;
    });
  }

}
