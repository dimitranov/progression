import { Component, OnInit, Input } from '@angular/core';

import { ExerciseNameFormatingPipe } from './../../pipes/exercise-name-formating.pipe';
import { ExerciseMovement } from './../../types/workout-type';
import { CapitalizePipe } from './../../pipes/capitalize.pipe';

@Component({
  selector: 'app-entity-grid-links',
  templateUrl: './entity-grid-links.component.html',
  styleUrls: ['./entity-grid-links.component.scss']
})
export class EntityGridLinksComponent implements OnInit {
  @Input() data: string[] | ExerciseMovement[];
  @Input() title;
  @Input() route;
  @Input() imageType;

  constructor() { }

  ngOnInit() {
  }

  checkType(val: string | ExerciseMovement): boolean {
    return typeof(val) === 'string';
  }

}
