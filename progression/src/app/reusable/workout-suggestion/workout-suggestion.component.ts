import { GroupDaysLeft } from './../../types/group-days-left';
import { Router } from '@angular/router';
import { WorkoutDecidingService } from './../../services/workout-deciding.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-workout-suggestion',
  templateUrl: './workout-suggestion.component.html',
  styleUrls: ['./workout-suggestion.component.scss']
})
export class WorkoutSuggestionComponent implements OnInit {
  @Input() groupArray;
  arrayForWorkoutSeuggestion: GroupDaysLeft[];
  major4Calculation = [];
  minior4Calculation = [];
  userDidWorkoutToday: boolean;
  constructor(
    private workoutServ: WorkoutDecidingService,
    private router: Router) {
      this.userDidWorkoutToday = true;
    }

  ngOnInit() {
    this.generateArrayForSuggestion();
    this.workoutServ.getWorkoutSesionOfToday()
      .subscribe(r => {
        this.userDidWorkoutToday =
        Array.isArray(r) && r.length === 0 ? false : true;
      });
  }


  generateArrayForSuggestion(): void {
    const tempArr  = [];
    for (const group of this.groupArray) {
      const difirenceBetweenTwoDates = Math.floor((new Date(group.readyDate).getTime() - new Date().getTime()) / 1000 / 60 / 60 / 24);
      tempArr.push({
        name: group.group,
        daysLeft: difirenceBetweenTwoDates
      });
    }
    this.arrayForWorkoutSeuggestion = tempArr.sort((a, b) => a.daysLeft - b.daysLeft).filter(o => o.daysLeft <= 0);
    const majG: string[] = ['chest', 'back', 'shoulders', 'legs'];
    const minG: string[] = ['biceps', 'triceps', 'abbs', 'traps', 'forearms', 'claves'];
    for (const obj of this.arrayForWorkoutSeuggestion) {
      switch (obj.name) {
        case majG[0]:
        case majG[1]:
        case majG[2]:
        case majG[3]:
          this.major4Calculation.push(obj);
          break;
        case minG[0]:
        case minG[1]:
        case minG[2]:
        case minG[3]:
        case minG[4]:
        case minG[5]:
          this.minior4Calculation.push(obj);
          break;
        default:
          break;
      }
    }
    console.log('arrayForWorkoutSeuggestion', this.arrayForWorkoutSeuggestion);
    console.log('major4Calculation', this.major4Calculation);
    console.log('minior4Calculation', this.minior4Calculation);
  }

}


