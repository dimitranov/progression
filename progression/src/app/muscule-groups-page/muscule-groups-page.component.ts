import { Component, OnInit } from '@angular/core';

import { CapitalizePipe } from './../pipes/capitalize.pipe';

@Component({
  selector: 'app-muscule-groups-page',
  templateUrl: './muscule-groups-page.component.html',
  styleUrls: ['./muscule-groups-page.component.css']
})
export class MusculeGroupsPageComponent implements OnInit {
  muscules: string[];
  constructor() {
    this.muscules = [
      'chest',
      'back',
      'shoulders',
      'legs',
      'biceps',
      'triceps',
      'traps',
      'abbs',
      'calves',
      'forearms'
    ];
  }

  ngOnInit() {
  }

}
