import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { WorkoutSuggestionComponent } from './../reusable/workout-suggestion/workout-suggestion.component';
import { CapitalizePipe } from './../pipes/capitalize.pipe';
import { WorkoutDecidingService } from './../services/workout-deciding.service';

declare var $: any;

@Component({
  selector: 'app-add-workout',
  templateUrl: './add-workout.component.html',
  styleUrls: ['./add-workout.component.scss']
})
export class AddWorkoutComponent implements OnInit, OnDestroy {
  workoutForm: FormGroup;
  initialWorkoutObj: {
    major: string,
    minior: string;
    additional: string;
  };
  formIsValid: boolean;
  isThereRequestForAddingWorkoutOnSameDay: boolean;
  majorGroupsArr: string[];
  allGroupsRecoveryArray_LOCALE;

  constructor(
    private workoutServ: WorkoutDecidingService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private ref: ChangeDetectorRef,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.formIsValid = false;
    this.isThereRequestForAddingWorkoutOnSameDay = false;
    this.majorGroupsArr = ['chest', 'back', 'shoulders', 'legs'];
  }

  ngOnInit(): void {
    this.createForm();
    this.allGroupsRecoveryArray_LOCALE = JSON.parse(localStorage.getItem('allGroupsRecoveryArray_LOCALE'));
    console.log(this.allGroupsRecoveryArray_LOCALE);
    this.onChangesMinior();
  }
  ngOnDestroy(): void {
    localStorage.removeItem(`allGroupsRecoveryArray_LOCALE`);
  }
  createForm(): void {
    this.workoutForm = this.fb.group({
      major: ['', Validators.required],
      minior: ['', Validators.required],
      additional: ''
    });
  }

  clearAdditional(): void {
    this.workoutForm.controls['additional'].setValue('');
    $('.additional_btn_wraper label').removeClass('active');
  }

  onChangesMinior(): void {
    this.workoutForm.get('minior').valueChanges.subscribe(minior => {
      this.ref.detectChanges();
      const additional = this.workoutForm.get('additional').value;
      if (additional === minior) {
        this.clearAdditional();
      }
    });
  }

  activateConfurmation(): void {
    if (this.workoutForm.status === 'VALID') {
      this.formIsValid = false;
    } else {
      this.formIsValid = true;
    }
  }

  calculateRecoveryDate(group: string): string {
    let recoveryDate: string;
    const today = new Date();
    const OneDayInMS: number = 1000 * 60 * 60 * 24;
    switch (group) {
      case 'chest':
      case 'back':
      case 'shoulders':
      case 'legs':
        recoveryDate = new Date(today.getTime() + OneDayInMS * 5).toString();
        break;
      case 'biceps':
      case 'triceps':
      case 'traps':
        recoveryDate = new Date(today.getTime() + OneDayInMS * 3).toString();
        break;
      case 'abbs':
      case 'calves':
      case 'forearms':
        recoveryDate = new Date(today.getTime() + OneDayInMS * 2).toString();
        break;
      default:
        break;
    }
    return group !== '' ? recoveryDate.substring(4, 15).split(' ').join('-') : '';
  }

  addWorkoutSesion(): void {
    if (this.workoutForm.status === 'VALID') {
      this.workoutServ.getWorkoutSesionOfToday()
        .subscribe(response => {
          if (Array.isArray(response) && response.length > 0) {
            this.isThereRequestForAddingWorkoutOnSameDay = true;
          } else {
            const temopOBj = {
              date: new Date().toString().substring(4, 15).split(' ').join('-'),
              session: [
                this.workoutForm.get('major').value,
                this.workoutForm.get('minior').value,
                this.workoutForm.get('additional').value
              ],
              recovery: [{
                  group: this.workoutForm.get('major').value,
                  readyDate: this.calculateRecoveryDate(this.workoutForm.get('major').value)
                },
                {
                  group: this.workoutForm.get('minior').value,
                  readyDate: this.calculateRecoveryDate(this.workoutForm.get('minior').value)
                },
                {
                  group: this.workoutForm.get('additional').value,
                  readyDate: this.calculateRecoveryDate(this.workoutForm.get('additional').value)
                }
              ],
              completed: false,
            };
            this.workoutServ.addWorkout(temopOBj)
              .subscribe(workoutObj => {
                this.toastr.info('Workouts started.', 'Time to train!', {
                  progressBar: true,
                  timeOut: 4000,
                });
                this.router.navigate(['/session']);
              });
          }
        });
    } else {
      this.toastr.warning('Please select at leas Major and Minior group!', 'Warning!', {
        progressBar: true,
        timeOut: 4000,
      });
    }
  }

  setControll(value, group): void {
    this.workoutForm.controls[group].setValue(value);
  }

}

