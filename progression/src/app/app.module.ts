import { NoUserGuard } from './guards/no-user.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { FetchingDataService } from './services/fetching-data.service';
import { DecideWorkoutGuard } from './guards/decide-workout.guard';
import { UserGuard } from './guards/user.guard';
import { UserService } from './services/user.service';
import { ExercisePickingGuard } from './guards/exercise-picking.guard';
import { HttpClientModule } from '@angular/common/http';
import { WorkoutDecidingService } from './services/workout-deciding.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';
import { AddWorkoutComponent } from './add-workout/add-workout.component';
import { AppRoutingModule } from './/app-routing.module';
import { AddSessionComponent } from './add-session/add-session.component';
import { ExercisePickingService } from './services/exercise-picking.service';
import { ExerciseNameFormatingPipe } from './pipes/exercise-name-formating.pipe';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { ExerciseFromComponent } from './add-session/exercise-from/exercise-from.component';
import { DateBeautifyPipe } from './pipes/date-beautify.pipe';
import { UserDataAddingComponent } from './user-data-adding/user-data-adding.component';
import { HomeComponent } from './home/home.component';
import { HomePageService } from './services/home-page.service';
import { WorkoutPageComponent } from './workout-page/workout-page.component';
import { GroupExercisesViewComponent } from './workout-page/group-exercises-view/group-exercises-view.component';
import { HeaderComponent } from './header/header.component';
import { WorkoutPageRecoveryComponent } from './recovery/workout-page-recovery/workout-page-recovery.component';
import { SingleGroupRecoveryComponent } from './recovery/workout-page-recovery/single-group-recovery/single-group-recovery.component';
import { HomeClassGroupStatePipe } from './pipes/home-class-group-state.pipe';
import { DateOnlyDatePipe } from './pipes/date-only-date.pipe';
import { ExpForStatePipe } from './pipes/exp-for-state.pipe';
import {PopoverModule} from 'ngx-popover';
import { MusculeGroupsPageComponent } from './muscule-groups-page/muscule-groups-page.component';
import { ExercisesPageComponent } from './exercises-page/exercises-page.component';
import { EntityGridLinksComponent } from './reusable/entity-grid-links/entity-grid-links.component';
import { MeasurementsFormComponent } from './user-data-adding/measurements-form/measurements-form.component';
import { WorkoutSuggestionComponent } from './reusable/workout-suggestion/workout-suggestion.component';
import { LatestWorkoutsComponent } from './home/latest-workouts/latest-workouts.component';
import { UserProgressionComponent } from './user-progression/user-progression.component';
import { MeasurementsChartComponent } from './user-progression/measurements-chart/measurements-chart.component';
import { MesurementsFormManagerComponent } from './home/mesurements-form-manager/mesurements-form-manager.component';
import { ExerciseProgressComponent } from './exercises-page/exercise-progress/exercise-progress.component';
import { ExerciseIllustrationComponent } from './exercises-page/exercise-progress/exercise-illustration/exercise-illustration.component';
import { ExerciseDetailsComponent } from './exercises-page/exercise-progress/exercise-details/exercise-details.component';
import { ExerImpactComponent } from './exercises-page/exercise-progress/exercise-details/exer-impact/exer-impact.component';
import { BarRatingModule } from 'ngx-bar-rating';
import { ExerciseInstancesComponent } from './exercises-page/exercise-progress/exercise-instances/exercise-instances.component';
import { ExerInstanceComponent } from './exercises-page/exercise-progress/exercise-instances/exer-instance/exer-instance.component';
import { ExerciseChartComponent } from './exercises-page/exercise-progress/exercise-chart/exercise-chart.component';
import { MusculeDevelopmentComponent } from './muscule-development/muscule-development.component';
import { MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatListModule,
  MatDividerModule,
  MatSnackBarModule,
  MatChipsModule,
  MatDialogModule,
  MatStepperModule,
  MatSelectModule
  } from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { LoaderComponent } from './reusable/loader/loader.component';
import { FooterComponent } from './footer/footer.component';
import { CalorieValuesComponent } from './home/calorie-values/calorie-values.component';
import { ToastrModule } from 'ngx-toastr';
import { CalendarWorkoutsComponent } from './home/calendar-workouts/calendar-workouts.component';

@NgModule({
  declarations: [
    AppComponent,
    AddWorkoutComponent,
    AddSessionComponent,
    ExerciseNameFormatingPipe,
    CapitalizePipe,
    ExerciseFromComponent,
    DateBeautifyPipe,
    UserDataAddingComponent,
    HomeComponent,
    WorkoutPageComponent,
    GroupExercisesViewComponent,
    HeaderComponent,
    WorkoutPageRecoveryComponent,
    SingleGroupRecoveryComponent,
    HomeClassGroupStatePipe,
    DateOnlyDatePipe,
    ExpForStatePipe,
    MusculeGroupsPageComponent,
    ExercisesPageComponent,
    EntityGridLinksComponent,
    MeasurementsFormComponent,
    WorkoutSuggestionComponent,
    LatestWorkoutsComponent,
    UserProgressionComponent,
    MeasurementsChartComponent,
    MesurementsFormManagerComponent,
    ExerciseProgressComponent,
    ExerciseIllustrationComponent,
    ExerciseDetailsComponent,
    ExerImpactComponent,
    ExerciseInstancesComponent,
    ExerInstanceComponent,
    ExerciseChartComponent,
    MusculeDevelopmentComponent,
    LoaderComponent,
    FooterComponent,
    CalorieValuesComponent,
    CalendarWorkoutsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({ preventDuplicates: true}),
    ReactiveFormsModule,
    AppRoutingModule,
    PopoverModule,
    ChartsModule,
    BarRatingModule,
    NgxChartsModule,
    MatButtonModule,
    MatCardModule,
    MatTooltipModule,
    MatToolbarModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatDividerModule,
    MatSnackBarModule,
    MatChipsModule,
    MatDialogModule,
    MatSelectModule,
    MatStepperModule,
  ],
  providers: [
    WorkoutDecidingService,
    ExercisePickingService,
    ExercisePickingGuard,
    UserService,
    UserGuard,
    NoUserGuard,
    DecideWorkoutGuard,
    HomePageService,
    FetchingDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
