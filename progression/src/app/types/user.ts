export class User {
  name: string;
  age: number;
  weight: number;
  height: number;
  bim: number;
  goals: string;
  caloriesLose: number;
  caloriesKeep: number;
  caloriesGain: number;
}

export class UserMeasurements {
  neck: number;
  chest: number;
  arms: number;
  waist: number;
  hip: number;
  thigh: number;
  date: string;
  calves: number;
  id: number;
}

export class UserGoals {
  goalShort: string;
  goal: string;
  value: number;
}
