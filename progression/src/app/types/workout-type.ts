export class Workout {
    id: number;
    date: string;
    session: string[];
    recovery: {
        group: string;
        readyDate: string;
    }[];
    completed: boolean;
}

export class MusculeGroupSesion {
    id: number;
    date: string;
    exercises: string[];
}

export class Exercise {
    id: number;
    name: string; // bench press
    date: string; // 4/10/2018
    weight: number; // 70
    reps: number; // 12
    target: string;
}

export class ExerciseMovement {
    target: string;
    name: string;
    imageURL: string;
    impact: number;
}

export class GroupSpecificExercise {
    name: string;
    impact: number;
    target: string;
}

export class Recovery {
    group: string;
    date: string;
    readyDate: string;
}
