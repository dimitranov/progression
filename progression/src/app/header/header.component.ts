import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isMobile: boolean;
  constructor() {
    this.isMobile = window.innerWidth > 650 ? false : true;
  }

  ngOnInit(): void {}
  onResize(event) {
    this.isMobile = event.target.innerWidth > 650 ? false : true;
  }

}

