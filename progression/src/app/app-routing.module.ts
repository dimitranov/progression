import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { NoUserGuard } from './guards/no-user.guard';
import { MusculeDevelopmentComponent } from './muscule-development/muscule-development.component';
import { ExerciseProgressComponent } from './exercises-page/exercise-progress/exercise-progress.component';
import { UserProgressionComponent } from './user-progression/user-progression.component';
import { ExercisesPageComponent } from './exercises-page/exercises-page.component';
import { MusculeGroupsPageComponent } from './muscule-groups-page/muscule-groups-page.component';
import { WorkoutPageComponent } from './workout-page/workout-page.component';
import { DecideWorkoutGuard } from './guards/decide-workout.guard';
import { HomeComponent } from './home/home.component';
import { UserGuard } from './guards/user.guard';
import { UserDataAddingComponent } from './user-data-adding/user-data-adding.component';
import { ExercisePickingGuard } from './guards/exercise-picking.guard';
import { AddSessionComponent } from './add-session/add-session.component';
import { AddWorkoutComponent } from './add-workout/add-workout.component';


const routes: Routes = [{
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'decide-workout',
    canActivate: [DecideWorkoutGuard],
    component: AddWorkoutComponent
  },
  {
    path: 'home',
    canActivate: [NoUserGuard],
    component: HomeComponent
  },
  {
    path: 'session',
    canActivate: [ExercisePickingGuard],
    component: AddSessionComponent
  },
  {
    path: 'add-user',
    canActivate: [UserGuard],
    component: UserDataAddingComponent
  },
  {
    path: 'workouts/:date',
    canActivate: [NoUserGuard],
    component: WorkoutPageComponent
  },
  {
    path: 'muscule-groups',
    canActivate: [NoUserGuard],
    component: MusculeGroupsPageComponent
  },
  {
    path: 'muscule-groups/:group',
    canActivate: [NoUserGuard],
    component: MusculeDevelopmentComponent
  },
  {
    path: 'exercises',
    canActivate: [NoUserGuard],
    component: ExercisesPageComponent
  },
  {
    path: 'exercises/:exercise',
    canActivate: [NoUserGuard],
    component: ExerciseProgressComponent
  },
  {
    path: 'my-progression',
    canActivate: [NoUserGuard],
    component: UserProgressionComponent
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

