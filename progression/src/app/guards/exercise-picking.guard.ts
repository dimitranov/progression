import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { WorkoutDecidingService } from './../services/workout-deciding.service';
import { ExercisePickingService } from './../services/exercise-picking.service';

@Injectable()
export class ExercisePickingGuard implements CanActivate {
  constructor(private workoutDeciding: WorkoutDecidingService, private router: Router) {}

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot):
    Observable < boolean > | Promise < boolean > | boolean {

      return this.workoutDeciding.getWorkoutSesionOfToday().map(
        resp => {
          if (Array.isArray(resp) && resp.length === 0) {
            console.log('Accsess Denayed', resp);
            this.router.navigate(['/decide-workout']);
            return false;
          } else if (resp[0].completed) {
            console.log('Accsess Denayed', resp);
            this.router.navigate(['/home']);
            return false;
          } else {
            console.log('Accsess Alowed', resp);
            return true;
          }
        });
    }
}

