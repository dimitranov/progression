import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { UserService } from '../services/user.service';

@Injectable()
export class UserGuard implements CanActivate {
  constructor(private user: UserService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable < boolean > | Promise < boolean > | boolean {
    return this.user.getUser()
      .map(res => {
        if (Object.keys(res).length === 0 && res.constructor === Object) {
          console.log('Access awoled there is no object', res);
          return true;
        } else {
          console.log('Access denay there is an object', res);
          this.router.navigate(['/home']);
          return false;
        }
      });
  }
}

