import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { WorkoutDecidingService } from './../services/workout-deciding.service';
import { ExercisePickingService } from './../services/exercise-picking.service';

@Injectable()
export class DecideWorkoutGuard implements CanActivate {
  constructor(private workoutDeciding: WorkoutDecidingService, private router: Router) {}

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot):
    Observable < boolean > | Promise < boolean > | boolean {


      return this.workoutDeciding.getWorkoutSesionOfToday().map(
        resp => {
          if (Array.isArray(resp) && resp.length === 0) {
            // console.log('Access Granted', resp);
            return true;
          } else {
            // console.log('Access Denied', resp);
            this.router.navigate(['/decide-workout']);
            return false;
          }
        });
    }
}

