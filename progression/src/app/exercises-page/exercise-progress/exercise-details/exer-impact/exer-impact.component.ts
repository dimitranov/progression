import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-exer-impact',
  templateUrl: './exer-impact.component.html',
  styleUrls: ['./exer-impact.component.css']
})
export class ExerImpactComponent implements OnInit {
  @Input() conut: number;
  impactUnitsArray = [];
  constructor() { }

  ngOnInit() {
    for (let i = 0; i <= 5; i++) {
      if (i < this.conut ) {
        this.impactUnitsArray[i] = '#4dbc83';
      } else {
        this.impactUnitsArray[i] = '#a7a7a7';
      }
    }
  }

}
