import { Component, OnInit, Input } from '@angular/core';
import { ExerciseMovement } from '../../../types/workout-type';

@Component({
  selector: 'app-exercise-details',
  templateUrl: './exercise-details.component.html',
  styleUrls: ['./exercise-details.component.css']
})
export class ExerciseDetailsComponent implements OnInit {
  @Input() data: ExerciseMovement;

  constructor() { }

  ngOnInit() {
  }

}
