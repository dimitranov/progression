import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { Exercise, ExerciseMovement } from './../../types/workout-type';
import { FetchingDataService } from './../../services/fetching-data.service';

@Component({
  selector: 'app-exercise-progress',
  templateUrl: './exercise-progress.component.html',
  styleUrls: ['./exercise-progress.component.css']
})
export class ExerciseProgressComponent implements OnInit {

  exerciseMatrix: Exercise[][];
  uniqueDates: string[];
  exerciseDetails: ExerciseMovement;

  exercise: string;
  allDataIsHere = false;

  constructor(
    private fetch: FetchingDataService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.exercise = this.route.snapshot.paramMap.get('exercise');
    this.exerciseMatrix = [];
   }

  ngOnInit(): void {
    this.fetchAllExercisePractices(this.exercise);
  }

  findAllUniqueDates(arr: any[]): string[] {
    const obj = {};
    for (const item of arr) {
      obj[item.date] = true;
    }
    return Object.keys(obj);
  }

  fetchAllExercisePractices(exercise: string): void {
    this.fetch.getAllExercisePractices(exercise)
      .subscribe(response => {
        this.uniqueDates = this.findAllUniqueDates(response);
        this.uniqueDates.forEach(date => {
          this.exerciseMatrix.push(response.filter(obj => obj.date === date));
        });
        this.fetchExericseData(this.exercise);
         console.log(this.exerciseMatrix);
      });
  }

  fetchExericseData(exercise: string): void {
    this.fetch.getExerciseData(exercise)
      .subscribe(response => {
        this.exerciseDetails = response[0];
        this.allDataIsHere = true;
      });
  }

}
