import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-exercise-illustration',
  templateUrl: './exercise-illustration.component.html',
  styleUrls: ['./exercise-illustration.component.css']
})
export class ExerciseIllustrationComponent implements OnInit {
  @Input() url: string;

  constructor() { }

  ngOnInit() {
  }

}
