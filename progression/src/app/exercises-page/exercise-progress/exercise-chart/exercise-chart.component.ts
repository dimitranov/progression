import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-exercise-chart',
  templateUrl: './exercise-chart.component.html',
  styleUrls: ['./exercise-chart.component.css']
})
export class ExerciseChartComponent implements OnInit, OnChanges {
  @Input() exerciseMatrix; // [][];
  @Input() fromMuscules;
  /*  data; // [{data:[1,2,4], label: data,}]
      label; // string[] */ // dati
  data: any[];
  label = [];

  constructor() {}

  ngOnInit(): void {
    this.generateLabels();
    this.generateChartData();
  }

  ngOnChanges(): void {
    console.log(this.exerciseMatrix);
  }

  generateLabels(): void {
    for (const arr of this.exerciseMatrix) {
      const date = arr[0].date.split('-')[0] + ' ' + arr[0].date.split('-')[1];
      this.label.push(date);
    }
  }

  generateChartData(): void {
    const tempDataArr = [];
    for (const arr of this.exerciseMatrix) {
      let sum = 0;
      for (const obj of arr) {
        sum += +obj.reps + +obj.weight;
      }
      tempDataArr.push(sum);
    }
    this.data = [{
      data: tempDataArr,
      label: this.fromMuscules ? this.exerciseMatrix[0][0].name : 'Score'
    }];
    console.log(this.data);
  }

}

