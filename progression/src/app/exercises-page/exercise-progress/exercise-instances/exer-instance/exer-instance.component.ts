import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-exer-instance',
  templateUrl: './exer-instance.component.html',
  styleUrls: ['./exer-instance.component.css']
})
export class ExerInstanceComponent implements OnInit {
  @Input() data;

  constructor() { }

  ngOnInit() {
  }

}
