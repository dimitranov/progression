import { Exercise } from './../../../types/workout-type';
import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-exercise-instances',
  templateUrl: './exercise-instances.component.html',
  styleUrls: ['./exercise-instances.component.css']
})
export class ExerciseInstancesComponent implements OnInit {
  @Input() exerciseMatrix; // [][]
  @Input() single;

  constructor() {
  }

  ngOnInit() {
  }

}


