import { Component, OnInit } from '@angular/core';

import { Exercise, ExerciseMovement } from './../types/workout-type';
import { FetchingDataService } from './../services/fetching-data.service';

@Component({
  selector: 'app-exercises-page',
  templateUrl: './exercises-page.component.html',
  styleUrls: ['./exercises-page.component.css']
})
export class ExercisesPageComponent implements OnInit {
  data: ExerciseMovement[];
  muscules: string[];
  omegaArray: ExerciseMovement[][];
  loadedStatus = false;

  constructor(private fetch: FetchingDataService) {
    this.muscules = [
      'chest',
      'back',
      'shoulders',
      'legs',
      'biceps',
      'triceps',
      'traps',
      'abbs',
      'calves',
      'forearms'
    ];
    this.omegaArray = [];
  }

  ngOnInit() {
    this.getExercises();
  }
  getExercises(): void {
    this.fetch.getAllExercisesData()
      .subscribe(res => {
        this.data = res;
        this.gonerateFiltratedArrayBasedonTarget();
        this.loadedStatus = true;
      });
  }
  gonerateFiltratedArrayBasedonTarget(): void {
    for (let i = 0; i < this.muscules.length; i++) {
      this.omegaArray.push(this.data.filter(obj => obj.target === this.muscules[i]));
    }
  }

}
